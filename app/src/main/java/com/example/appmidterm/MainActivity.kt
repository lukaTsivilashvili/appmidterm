package com.example.appmidterm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appmidterm.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setTheme(R.style.Theme_AppMidterm)

        setContentView(binding.root)
    }


}