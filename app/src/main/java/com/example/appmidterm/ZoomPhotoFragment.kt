package com.example.appmidterm

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.appmidterm.databinding.FragmentZoomPhotoBinding


class ZoomPhotoFragment : Fragment() {

    private lateinit var binding: FragmentZoomPhotoBinding

    val args: ZoomPhotoFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentZoomPhotoBinding.inflate(inflater, container, false)
        setImage()
        return binding.root

    }

    private fun setImage() {

        val photoUrl = args.url

        Glide.with(this)
            .load(photoUrl)
            .placeholder(R.mipmap.ic_launcher)
            .into(binding.zoomImageView)

    }

}