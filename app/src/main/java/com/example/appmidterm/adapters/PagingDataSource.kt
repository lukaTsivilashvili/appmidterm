package com.example.appmidterm.adapters

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.appmidterm.models.Photo
import com.example.appmidterm.networking.RetrofitInstance
import java.lang.Exception

class PagingDataSource : PagingSource<Int, Photo>() {
    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Photo> {

        try {

            val currentLoadingKey = params.key ?: 1

            val response = RetrofitInstance.service().getRecentPhotos(currentLoadingKey, "popular")

            val responseData = mutableListOf<Photo>()
            val data = response.body() ?: emptyList()
            responseData.addAll(data)

            val prevKey = if (currentLoadingKey == 1) null else currentLoadingKey - 1

            return LoadResult.Page(
                data = responseData,
                prevKey = prevKey,
                nextKey = if(data.isEmpty()) null else currentLoadingKey + 1
            )

        }catch (e:Exception){
            return LoadResult.Error(e)
        }
    }
}