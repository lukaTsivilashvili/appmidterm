package com.example.appmidterm.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmidterm.databinding.ItemPhotoBinding
import com.example.appmidterm.models.Photo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class PhotoAdapter(private val context: Context) :
    PagingDataAdapter<Photo, PhotoAdapter.UserViewHolder>(PhotoComparator) {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var db: DatabaseReference


    inner class UserViewHolder(private val binding: ItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {

            mAuth = FirebaseAuth.getInstance()
            db = FirebaseDatabase.getInstance().getReference("UserInfo")

            val photo: ImageView = binding.photo
            val favBtn: ImageButton = binding.BtnFav

            val photoItem = getItem(absoluteAdapterPosition)

            Glide.with(context)
                .load(photoItem?.url?.regular)
                .placeholder(ColorDrawable(Color.parseColor(photoItem?.color)))
                .into(photo)

            favBtn.setOnClickListener {

                val photosInfo =
                    Photo(photoItem!!.id, photoItem.color, photoItem.description, photoItem.url)

                val time = System.currentTimeMillis()

                if (mAuth.currentUser?.uid != null) {

                    db.child(mAuth.currentUser?.uid!!).child("$time").setValue(photosInfo.url?.regular)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
                            } else {
                                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                            }
                        }

                }


            }
        }
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            ItemPhotoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    object PhotoComparator : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
            return oldItem == newItem
        }

    }


}

