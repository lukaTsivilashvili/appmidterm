package com.example.appmidterm.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.appmidterm.databinding.ItemFavouritesBinding
import com.example.appmidterm.models.DataGlideModel
import com.example.appmidterm.ui.BottomNavFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase


class RecyclerFavouritesAdapter :
    RecyclerView.Adapter<RecyclerFavouritesAdapter.FavouritesViewHolder>() {

    var photos = mutableListOf<String>()

    inner class FavouritesViewHolder(private val binding: ItemFavouritesBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var photoModel: String
        private var dbRef = FirebaseDatabase.getInstance().getReference("UserInfo")
        private var userId = FirebaseAuth.getInstance().currentUser?.uid

        fun bind() {

            photoModel = photos[absoluteAdapterPosition]
            binding.image = DataGlideModel(photoModel.drop(15))

            var key = photoModel.split("=")[0].drop(1)


            binding.photoFavs.setOnClickListener {

                val action = BottomNavFragmentDirections.actionBottomNavFragmentToZoomPhotoFragment(
                    photoModel.drop(15)
                )
                Navigation.findNavController(it).navigate(action)

            }


            binding.btnRemove.setOnClickListener {

                userId?.let { it1 -> dbRef.child(it1).child(key).removeValue() }
                notifyDataSetChanged()

            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouritesViewHolder {
        val itemView =
            ItemFavouritesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = FavouritesViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: FavouritesViewHolder, position: Int) {

        holder.bind()


    }

    override fun getItemCount() = photos.size

    fun setData(photos: MutableList<String>) {
        this.photos = photos
        notifyDataSetChanged()
    }

}
