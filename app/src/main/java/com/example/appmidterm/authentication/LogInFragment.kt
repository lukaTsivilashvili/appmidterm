package com.example.appmidterm.authentication

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.appmidterm.R
import com.example.appmidterm.databinding.FragmentLogInBinding
import com.example.appmidterm.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth

class LogInFragment : Fragment() {

    private lateinit var binding: FragmentLogInBinding
    private lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLogInBinding.inflate(inflater, container, false)
        mAuth = FirebaseAuth.getInstance()
        instaLogIn()
        logIn()
        return binding.root
    }


    private fun instaLogIn() {

        if (mAuth.currentUser != null) {
            findNavController().navigate(R.id.action_logInFragment_to_bottomNavFragment)
        }

    }

    private fun logIn() {
        binding.BtnLogIn.setOnClickListener {
            val email = binding.EtLogInEmail.text.toString()
            val password = binding.EtLogInPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(
                    requireActivity(),
                    "One or more fields are empty",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        findNavController().navigate(R.id.action_logInFragment_to_bottomNavFragment)
                    } else {
                        Toast.makeText(requireActivity(), "Error !!!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        binding.BtnLogRegister.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_registerFragment)
        }
    }

}