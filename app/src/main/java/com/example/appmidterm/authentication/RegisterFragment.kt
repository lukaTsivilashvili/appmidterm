package com.example.appmidterm.authentication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.appmidterm.R
import com.example.appmidterm.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth

class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding
    private lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        mAuth = FirebaseAuth.getInstance()
        regUser()
        return binding.root
    }


    private fun regUser() {


        binding.BtnRegLogIn.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.BtnRegister.setOnClickListener {

            val mail = binding.EtRegisterEmail.text.toString()
            val password = binding.EtRegisterPassword.text.toString()

            if (mail.isEmpty() || password.isEmpty()) {
                Toast.makeText(
                    requireActivity(),
                    "One or more fields are empty",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                mAuth.createUserWithEmailAndPassword(mail, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        findNavController().navigate(R.id.action_registerFragment_to_bottomNavFragment)
                    } else {
                        Toast.makeText(requireActivity(), "Error !!!", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }

    }

}