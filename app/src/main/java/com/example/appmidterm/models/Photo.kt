package com.example.appmidterm.models

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

data class Photo(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("color")
    val color: String? = null,
    @SerializedName("alt_description")
    val description: String? = null,
    @SerializedName("urls")
    val url: PhotoUrl? = null
)