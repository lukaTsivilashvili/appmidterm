package com.example.appmidterm.models

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

data class PhotoUrl(
    @SerializedName("full")
    val full: String? = null,
    @SerializedName("regular")
    val regular: String? = null
)