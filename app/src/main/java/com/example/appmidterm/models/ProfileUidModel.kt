package com.example.appmidterm.models

data class ProfileUidModel(

    val mail: String,
    val uid: String

)
