package com.example.appmidterm.networking

import com.example.appmidterm.models.Photo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {


    @GET("photos")
    suspend fun getRecentPhotos(
        @Query("page") page: Int,
        @Query("order_by") order: String,
        @Query("client_id") clientId: String ="aQXwBXedcGgycJchO3glaDP06nMNnECWhPy1WQTfOfA"
    ): Response<MutableList<Photo>>

}