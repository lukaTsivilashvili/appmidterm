package com.example.appmidterm.ui

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.appmidterm.R
import com.example.appmidterm.adapters.ViewPagerAdapter
import com.example.appmidterm.databinding.FragmentBottomNavBinding
import com.example.appmidterm.ui.favourites.FavouritesFragment
import com.example.appmidterm.ui.photos.PhotosFragment
import com.example.appmidterm.ui.profile.ProfileFragment


class BottomNavFragment : Fragment() {

    private var binding: FragmentBottomNavBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        if (binding == null){


        binding = FragmentBottomNavBinding.inflate(inflater, container, false)
        initViewPager()

        }

        return binding?.root
    }

    private fun initViewPager() {
        val frags = mutableListOf<Fragment>()

        frags.add(PhotosFragment())
        frags.add(FavouritesFragment())
        frags.add(ProfileFragment())

        binding?.viewPager2?.adapter = ViewPagerAdapter(frags, childFragmentManager, lifecycle)
        binding?.viewPager2?.offscreenPageLimit = 3

        binding?.viewPager2?.isSaveEnabled = false

        binding?.viewPager2?.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                selectItem(position)
            }
        })

        binding?.navView?.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_photos -> binding?.viewPager2?.currentItem = 0
                R.id.navigation_favourites -> binding?.viewPager2?.currentItem = 1
                R.id.navigation_profile -> binding?.viewPager2?.currentItem = 2
            }
            true
        }
    }

    private fun selectItem(position: Int) {
        binding?.navView?.menu?.getItem(position)?.isChecked = true
    }



}