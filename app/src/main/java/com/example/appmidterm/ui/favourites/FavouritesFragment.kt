package com.example.appmidterm.ui.favourites

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appmidterm.adapters.RecyclerFavouritesAdapter
import com.example.appmidterm.databinding.FragmentFavouritesBinding
import com.example.appmidterm.models.Photo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class FavouritesFragment : Fragment() {

    private lateinit var binding: FragmentFavouritesBinding
    private lateinit var myAdapter: RecyclerFavouritesAdapter

    private lateinit var mAuth: FirebaseAuth
    private lateinit var db: DatabaseReference

    private lateinit var photosList: MutableList<Photo>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavouritesBinding.inflate(inflater, container, false)
        photosList = mutableListOf()
        mAuth = FirebaseAuth.getInstance()
        load()
        initRecycler()
        return binding.root
    }

    private fun load() {


        val reference: DatabaseReference =
            FirebaseDatabase.getInstance().reference.child("UserInfo")
                .child(mAuth.currentUser?.uid!!)

        reference.addValueEventListener(object :
            ValueEventListener {

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, "Error!!!!", Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {

                val items = snapshot.value.toString().dropLast(1)

                val photoList = items.split(",")


                d("testD", photosList.toString())
                myAdapter.setData(photoList as MutableList<String>)

            }
        })
    }


    private fun initRecycler() {

        myAdapter = RecyclerFavouritesAdapter()

        db = FirebaseDatabase.getInstance().reference
        binding.myRecyclerFavourites.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.myRecyclerFavourites.adapter = myAdapter

    }


}