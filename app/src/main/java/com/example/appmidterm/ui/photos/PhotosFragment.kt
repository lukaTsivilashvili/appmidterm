package com.example.appmidterm.ui.photos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appmidterm.adapters.PhotoAdapter
import com.example.appmidterm.databinding.FragmentPhotosBinding

class PhotosFragment : Fragment() {

    private val viewModel: PhotosViewModel by viewModels()
    private lateinit var myAdapter: PhotoAdapter
    private var binding: FragmentPhotosBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        if (binding == null) {
            binding = FragmentPhotosBinding.inflate(inflater, container, false)

            init()

        }

        return binding?.root
    }

    private fun init() {
        initRecycler()
        observe()
    }

    private fun initRecycler() {

        myAdapter = PhotoAdapter(requireContext())

        binding?.myRecyclerPhotos?.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            setHasFixedSize(true)
            adapter = myAdapter
        }

        myAdapter.addLoadStateListener {
            binding?.myProgressBar?.isVisible = it.refresh is LoadState.Loading
        }

    }

    private fun observe() {

        viewModel.listPhoto.observe(viewLifecycleOwner, {
            myAdapter.submitData(lifecycle, it)
        })
    }


}