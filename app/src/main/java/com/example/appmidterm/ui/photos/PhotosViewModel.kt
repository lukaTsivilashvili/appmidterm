package com.example.appmidterm.ui.photos

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.example.appmidterm.adapters.PagingDataSource


class PhotosViewModel : ViewModel() {

    val listPhoto = Pager(PagingConfig(pageSize = 30)) {
        PagingDataSource()
    }.liveData


}