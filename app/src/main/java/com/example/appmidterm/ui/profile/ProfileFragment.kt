package com.example.appmidterm.ui.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.appmidterm.R
import com.example.appmidterm.databinding.FragmentProfileBinding
import com.example.appmidterm.models.ProfileUidModel
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        mAuth = FirebaseAuth.getInstance()
        initProfileAuth()
        return binding.root
    }

    private fun logOut() {

        binding.button.setOnClickListener {
            mAuth.signOut()
            findNavController().navigate(R.id.action_bottomNavFragment_to_logInFragment)

        }
    }

    private fun initProfileAuth() {
        logOut()

        binding.user = ProfileUidModel(mAuth.currentUser?.email!!, mAuth.currentUser?.uid!!)


    }

}